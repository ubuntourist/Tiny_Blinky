EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 2
Title "Tiny_Blinky"
Date "2020-07-06"
Rev "ver1"
Comp "Hackaday U."
Comment1 "Introduction to KiCad and FreeCAD"
Comment2 "Kevin Cole"
Comment3 "Anool Mahidharia"
Comment4 ""
$EndDescr
$Comp
L tiny_blinky:SW_Push SW1
U 1 1 5F03FA86
P 3250 1400
F 0 "SW1" V 3204 1548 50  0000 L CNN
F 1 "SW" V 3295 1548 50  0000 L CNN
F 2 "tiny_blinky:SW_PUSH_6mm_H5mm" H 3250 1600 50  0001 C CNN
F 3 "~" H 3250 1600 50  0001 C CNN
	1    3250 1400
	0    1    1    0   
$EndComp
$Comp
L tiny_blinky:R R1
U 1 1 5F04018D
P 3250 2800
F 0 "R1" V 3250 2750 50  0000 L CNN
F 1 "10k" H 3300 2950 50  0000 L CNN
F 2 "tiny_blinky:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 3180 2800 50  0001 C CNN
F 3 "~" H 3250 2800 50  0001 C CNN
	1    3250 2800
	1    0    0    -1  
$EndComp
$Comp
L tiny_blinky:R R2
U 1 1 5F04183B
P 4200 2300
F 0 "R2" V 4200 2300 50  0000 C CNN
F 1 "330E" V 4100 2300 50  0000 C CNN
F 2 "tiny_blinky:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 4130 2300 50  0001 C CNN
F 3 "~" H 4200 2300 50  0001 C CNN
	1    4200 2300
	0    1    1    0   
$EndComp
Wire Wire Line
	3250 950  3250 1200
Wire Wire Line
	3250 2950 3250 3100
Wire Wire Line
	3250 950  4100 950 
Wire Wire Line
	4100 950  4100 2000
Wire Wire Line
	4100 2000 4600 2000
Wire Wire Line
	4100 3100 4100 2500
Wire Wire Line
	4100 2500 4600 2500
Wire Wire Line
	4350 2300 4600 2300
$Comp
L tiny_blinky:GND #PWR03
U 1 1 5F04BB7C
P 3250 3100
F 0 "#PWR03" H 3250 2850 50  0001 C CNN
F 1 "GND" H 3255 2927 50  0000 C CNN
F 2 "" H 3250 3100 50  0001 C CNN
F 3 "" H 3250 3100 50  0001 C CNN
	1    3250 3100
	1    0    0    -1  
$EndComp
$Comp
L tiny_blinky:GND #PWR04
U 1 1 5F04BF55
P 4100 3100
F 0 "#PWR04" H 4100 2850 50  0001 C CNN
F 1 "GND" H 4105 2927 50  0000 C CNN
F 2 "" H 4100 3100 50  0001 C CNN
F 3 "" H 4100 3100 50  0001 C CNN
	1    4100 3100
	1    0    0    -1  
$EndComp
$Comp
L tiny_blinky:WS2812B D1
U 1 1 5F0594AF
P 5600 2300
F 0 "D1" H 5944 2346 50  0000 L CNN
F 1 "WS2812B" H 5944 2255 50  0000 L CNN
F 2 "tiny_blinky:LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm" H 5650 2000 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf" H 5700 1925 50  0001 L TNN
	1    5600 2300
	1    0    0    -1  
$EndComp
$Comp
L tiny_blinky:WS2812B D2
U 1 1 5F0595D2
P 6900 2300
F 0 "D2" H 7244 2346 50  0000 L CNN
F 1 "WS2812B" H 7244 2255 50  0000 L CNN
F 2 "tiny_blinky:LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm" H 6950 2000 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf" H 7000 1925 50  0001 L TNN
	1    6900 2300
	1    0    0    -1  
$EndComp
$Comp
L tiny_blinky:WS2812B D3
U 1 1 5F059ECC
P 8200 2300
F 0 "D3" H 8544 2346 50  0000 L CNN
F 1 "WS2812B" H 8544 2255 50  0000 L CNN
F 2 "tiny_blinky:LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm" H 8250 2000 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf" H 8300 1925 50  0001 L TNN
	1    8200 2300
	1    0    0    -1  
$EndComp
$Comp
L tiny_blinky:Conn_01x03_Female J2
U 1 1 5F05AA8B
P 9300 2300
F 0 "J2" H 9328 2326 50  0000 L CNN
F 1 "Dout" H 9328 2235 50  0000 L CNN
F 2 "tiny_blinky:PinSocket_1x03_P2.54mm_Vertical" H 9300 2300 50  0001 C CNN
F 3 "~" H 9300 2300 50  0001 C CNN
	1    9300 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	8900 2200 8900 1850
Wire Wire Line
	8900 1850 8200 1850
Wire Wire Line
	5200 1850 5200 2000
Wire Wire Line
	5200 2000 5050 2000
Wire Wire Line
	8900 2200 9100 2200
Wire Wire Line
	5600 2000 5600 1850
Connection ~ 5600 1850
Wire Wire Line
	5600 1850 5200 1850
Wire Wire Line
	6900 2000 6900 1850
Connection ~ 6900 1850
Wire Wire Line
	6900 1850 5600 1850
Wire Wire Line
	8200 2000 8200 1850
Connection ~ 8200 1850
Wire Wire Line
	8200 1850 6900 1850
Wire Wire Line
	9100 2300 8500 2300
Wire Wire Line
	7900 2300 7200 2300
Wire Wire Line
	6600 2300 5900 2300
Wire Wire Line
	5300 2300 5050 2300
Wire Wire Line
	5050 2500 5200 2500
Wire Wire Line
	5200 2500 5200 2700
Wire Wire Line
	5200 2700 5600 2700
Wire Wire Line
	8900 2400 9100 2400
Wire Wire Line
	8900 2400 8900 2700
Wire Wire Line
	5600 2600 5600 2700
Connection ~ 5600 2700
Wire Wire Line
	5600 2700 6900 2700
Wire Wire Line
	6900 2600 6900 2700
Connection ~ 6900 2700
Wire Wire Line
	6900 2700 8200 2700
Wire Wire Line
	8200 2600 8200 2700
Connection ~ 8200 2700
Wire Wire Line
	8200 2700 8900 2700
Text Label 4600 2500 0    50   ~ 0
GND
Text Label 4600 2300 0    50   ~ 0
Din
Text Label 4600 2000 0    50   ~ 0
5V
Text Label 5050 2000 2    50   ~ 0
5V
Text Label 5050 2300 2    50   ~ 0
Din
Text Label 5050 2500 2    50   ~ 0
GND
$Comp
L tiny_blinky:Conn_02x03_Odd_Even J1
U 1 1 5F0754AC
P 3900 4450
F 0 "J1" H 3950 4767 50  0000 C CNN
F 1 "SPI PROG" H 3950 4676 50  0000 C CNN
F 2 "tiny_blinky:PinSocket_2x03_P2.54mm_Vertical" H 3900 4450 50  0001 C CNN
F 3 "~" H 3900 4450 50  0001 C CNN
	1    3900 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3700 4350 3250 4350
Wire Wire Line
	3700 4450 3250 4450
Wire Wire Line
	3700 4550 3250 4550
Wire Wire Line
	4650 4350 4200 4350
Wire Wire Line
	4650 4450 4200 4450
Wire Wire Line
	4650 4550 4200 4550
Text Label 4650 4350 2    50   ~ 0
5V
Text Label 4650 4550 2    50   ~ 0
GND
Text Label 4650 4450 2    50   ~ 0
MOSI
Text Label 3250 4350 0    50   ~ 0
MISO
Text Label 3250 4450 0    50   ~ 0
SCK
Text Label 3250 4550 0    50   ~ 0
RST
Text Label 2450 2400 2    50   ~ 0
RST
Text Label 2450 2100 2    50   ~ 0
SCK
Text Label 2450 2000 2    50   ~ 0
MISO
Text Label 2450 1900 2    50   ~ 0
MOSI
Wire Wire Line
	2050 2400 2450 2400
Wire Wire Line
	2050 2100 2450 2100
Wire Wire Line
	2050 2000 2450 2000
Wire Wire Line
	2050 1900 2450 1900
$Comp
L tiny_blinky:GND #PWR02
U 1 1 5F04AD86
P 1450 3100
F 0 "#PWR02" H 1450 2850 50  0001 C CNN
F 1 "GND" H 1455 2927 50  0000 C CNN
F 2 "" H 1450 3100 50  0001 C CNN
F 3 "" H 1450 3100 50  0001 C CNN
	1    1450 3100
	1    0    0    -1  
$EndComp
Connection ~ 1450 950 
$Comp
L tiny_blinky:+5V #PWR01
U 1 1 5F04A9C9
P 1450 950
F 0 "#PWR01" H 1450 800 50  0001 C CNN
F 1 "+5V" H 1465 1123 50  0000 C CNN
F 2 "" H 1450 950 50  0001 C CNN
F 3 "" H 1450 950 50  0001 C CNN
	1    1450 950 
	1    0    0    -1  
$EndComp
Text Label 2300 2300 0    50   ~ 0
LED
Text Label 2250 2200 0    50   ~ 0
SW_IN
Wire Wire Line
	1450 3100 1450 2800
Wire Wire Line
	1450 950  3250 950 
Wire Wire Line
	1450 1600 1450 950 
$Comp
L tiny_blinky:ATtiny85-20PU U1
U 1 1 5F03E0D4
P 1450 2200
F 0 "U1" H 1050 2750 50  0000 R CNN
F 1 "ATtiny85-20PU" V 1000 2450 50  0000 R CNN
F 2 "tiny_blinky:DIP-8_W7.62mm" H 1450 2200 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/atmel-2586-avr-8-bit-microcontroller-attiny25-attiny45-attiny85_datasheet.pdf" H 1450 2200 50  0001 C CNN
	1    1450 2200
	1    0    0    -1  
$EndComp
Connection ~ 3250 950 
Entry Wire Line
	2450 1900 2550 1800
Entry Wire Line
	2450 2000 2550 1900
Entry Wire Line
	2450 2100 2550 2000
Entry Wire Line
	2450 2400 2550 2500
Entry Wire Line
	4650 4450 4750 4350
Entry Wire Line
	3150 4250 3250 4350
Entry Wire Line
	3150 4350 3250 4450
Entry Wire Line
	3150 4450 3250 4550
Connection ~ 3250 2200
Wire Wire Line
	3250 2200 3250 2650
Wire Wire Line
	3250 1600 3250 2200
Wire Wire Line
	2050 2200 3250 2200
Wire Wire Line
	4050 2300 2050 2300
Wire Bus Line
	2550 3900 3150 3900
Wire Bus Line
	4750 3900 4750 4350
Connection ~ 3150 3900
Wire Bus Line
	3150 3900 4750 3900
$Sheet
S 7150 4500 1000 350 
U 5F0C9DA2
F0 "power_supply" 50
F1 "power_supply.sch" 50
F2 "5V" O L 7150 4600 50 
F3 "GND" O L 7150 4750 50 
$EndSheet
Wire Wire Line
	7150 4600 6850 4600
Wire Wire Line
	7150 4750 6850 4750
Text Label 6850 4600 0    50   ~ 0
5V
Text Label 6850 4750 0    50   ~ 0
GND
$Comp
L Mechanical:MountingHole_Pad H1
U 1 1 5F10FABB
P 5900 3500
F 0 "H1" H 6000 3503 50  0000 L CNN
F 1 "MountingHole_Pad" H 6000 3458 50  0001 L CNN
F 2 "tiny_blinky:MountingHole_3.2mm_M3_Pad_Via" H 5900 3500 50  0001 C CNN
F 3 "~" H 5900 3500 50  0001 C CNN
	1    5900 3500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR05
U 1 1 5F110BA4
P 5900 3600
F 0 "#PWR05" H 5900 3350 50  0001 C CNN
F 1 "GND" H 5905 3427 50  0000 C CNN
F 2 "" H 5900 3600 50  0001 C CNN
F 3 "" H 5900 3600 50  0001 C CNN
	1    5900 3600
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H2
U 1 1 5F114249
P 6250 3500
F 0 "H2" H 6350 3503 50  0000 L CNN
F 1 "MountingHole_Pad" H 6350 3458 50  0001 L CNN
F 2 "tiny_blinky:MountingHole_3.2mm_M3_Pad_Via" H 6250 3500 50  0001 C CNN
F 3 "~" H 6250 3500 50  0001 C CNN
	1    6250 3500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR06
U 1 1 5F114253
P 6250 3600
F 0 "#PWR06" H 6250 3350 50  0001 C CNN
F 1 "GND" H 6255 3427 50  0000 C CNN
F 2 "" H 6250 3600 50  0001 C CNN
F 3 "" H 6250 3600 50  0001 C CNN
	1    6250 3600
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H3
U 1 1 5F119798
P 6600 3500
F 0 "H3" H 6700 3503 50  0000 L CNN
F 1 "MountingHole_Pad" H 6700 3458 50  0001 L CNN
F 2 "tiny_blinky:MountingHole_3.2mm_M3_Pad_Via" H 6600 3500 50  0001 C CNN
F 3 "~" H 6600 3500 50  0001 C CNN
	1    6600 3500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR07
U 1 1 5F1197A2
P 6600 3600
F 0 "#PWR07" H 6600 3350 50  0001 C CNN
F 1 "GND" H 6605 3427 50  0000 C CNN
F 2 "" H 6600 3600 50  0001 C CNN
F 3 "" H 6600 3600 50  0001 C CNN
	1    6600 3600
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H4
U 1 1 5F1197AC
P 6950 3500
F 0 "H4" H 7050 3503 50  0000 L CNN
F 1 "MountingHole_Pad" H 7050 3458 50  0001 L CNN
F 2 "tiny_blinky:MountingHole_3.2mm_M3_Pad_Via" H 6950 3500 50  0001 C CNN
F 3 "~" H 6950 3500 50  0001 C CNN
	1    6950 3500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR08
U 1 1 5F1197B6
P 6950 3600
F 0 "#PWR08" H 6950 3350 50  0001 C CNN
F 1 "GND" H 6955 3427 50  0000 C CNN
F 2 "" H 6950 3600 50  0001 C CNN
F 3 "" H 6950 3600 50  0001 C CNN
	1    6950 3600
	1    0    0    -1  
$EndComp
$Comp
L tiny_blinky:LOGO_HacDC G1
U 1 1 5F10E604
P 10800 6850
F 0 "G1" H 10800 7360 60  0001 C CNN
F 1 "LOGO_HacDC" H 10800 7250 60  0001 C CNN
F 2 "tiny_blinky:logo_hacdc" H 10800 6850 50  0001 C CNN
F 3 "" H 10800 6850 50  0001 C CNN
	1    10800 6850
	1    0    0    -1  
$EndComp
Wire Bus Line
	3150 3900 3150 4450
Wire Bus Line
	2550 1800 2550 3900
$EndSCHEMATC
