# Hackaday U: [Introduction to KiCad and FreeCAD](https://hackaday.io/project/172360-introduction-to-kicad-and-freecad)

## 2020.07.06

The [Introduction to KiCad and
FreeCAD](https://hackaday.io/project/172360-introduction-to-kicad-and-freecad)
announcement page has a gallery of images as well as other useful
tidbits like prerequisites.

* [KiCad EDA](https://kicad-pcb.org/) is the offical site.

* [git repositories](https://github.com/KiCad) for symbols,
  footprints, and 3D packages:

    + https://github.com/KiCad/kicad-symbols
    + https://github.com/KiCad/kicad-footprints
    + https://github.com/KiCad/kicad-packages3D


* Starting from this sketch...

> <img src="./paper_sketch.jpg"
       alt="Hand-drawn schematic"
       title="Hand-drawn schematic"
       height=900
       style="height:900px;" />

* Start `kicad` and create a **New Project** named `tiny_blinky`. This
  creates a directory named `/tiny_blinky/` with three files:

        -rw-rw-r-- 1   51 Jul  6 19:38 tiny_blinky.kicad_pcb
        -rw-r--r-- 1  688 Jul  6 19:38 tiny_blinky.pro
        -rw-rw-r-- 1   72 Jul  6 19:38 tiny_blinky.sch

* The `.pro` is the _project_ file and references the other two --
  `.sch` I would guess is the schematic and `.kicad_pcb` the printed
  circuit board.

* The icons in the main window are listed from left to right in the
  order of use, sayeth our instructor.

    1. **eeschema** - Schematic Layout Editor
    2. **LibEdit** (?) - Symbol Editor
    3. **Pcbnew** - PCB Layout Editor
    4. **ModEdit** (?) - Footprint Editor
    5. **GerbView** - Gerber Viewer - to verify designs before shipping
    6. **Bitmap to Component Converter** - to create custom logos on the board
    7. **PCB Calculator** - several calculators, actually
    8. **PlEditor** - Page Layout Editor

* Our instructor sets `Prefrences -> Configure Paths` altering the
  system-wide default install paths `KICAD_SYMBOL_DIR`, `KISYSMOD` and
  `KISYS3DMOD`, which appear to correspond to **symbols**,
  **footprints** and **packages3D** respectively.

> | Environment Variable      | Path (`/usr/share/kicad/...`) | Purpose    |
> |:--------------------------|:------------------------------|:-----------|
> | `KICAD_SYMBOL_DIR`        | `.../library`                 | symbols    |
> | `KISYSMOD`                | `.../modules`                 | footprints |
> | `KISYS3DMOD`              | `.../modules/packages3d`      | packages3D |
> | `KICAD_USER_TEMPLATE_DIR` | `~/kicad/template`            | ???        |

* I **really** don't want to pollute system space with stuff not
  managed by a package manager. On closer examination of the default
  paths that came with KiCad, it seems the **footprints** are actually
  **modules** and the **packages3d** are a special class of module,
  contained in a subdirectory of the footprints directory. And for
  consistency, I've changed KICAD_USER_TEMPLATE_DIR too. So my
  overrides are:

> | Environment Variable      | Path (`~/.config/kicad/...`) | Purpose    |
> |:--------------------------|:-----------------------------|:-----------|
> | `KICAD_SYMBOL_DIR`        | `.../library`                | symbols    |
> | `KISYSMOD`                | `.../modules`                | footprints |
> | `KISYS3DMOD`              | `.../modules/packages3d`     | packages3D |
> | `KICAD_USER_TEMPLATE_DIR` | `.../template`               | ???        |

* Why the above? Because, unlike the symbols, footprints, and 3D
  information, scripts -- apparently -- do NOT have a configurable
  path and MUST be in a specific subdirectory of either `~/.kicad/`
  or `~/.config/kicad/`. I like `~/.config/` better.

* In **eeschema** use the second icon from the left (at the top) to
  **Edit Page Settings**. I'm tempted to edit the page size from
  **A4** to **Letter** but if we're working in millimeters, I may be
  better off with **A4**.

* And a curious thing: On the "paper" the comments appear in reverse(?)
  order: Comment 2 appears above Comment 1.

* He does not have all of his symbol libraries enabled and had to
  enable via **Preferences -> Manage Symbol Libraries**. Mine were
  all already enabled.

* Typing `ATtiny85` lists several. We want the DIP-8 (8-pin) which is
  **ATtiny85-20PU**.

* His symbol chooser shows a **footprint** preview in addition to the
  schematic symbol. I had to turn that on via:

```
        Preferences ->
          Preferences ->
            Eeschema ->
              [X] Show footprint previews in symbol chooser.
```
* `sw push` finds **SW_Push** a _Push button switch, generic, two pins_.

* `r` (or right click and choose) to **rotate** the switch clockwise.

* `[SPACE]` sets the **dx** and **dy** _relative_ coordinates for a
  given component, to 0. This makes it easy to align other components
  to 0,y or x,0 and have them line up vertically or horizontally.

* Eventually, he gets around to telling us he's using _IMPERIAL_
  coordinates. Nnnnaturally! (I probably should have noticed sooner.)
  I'm finally getting used to doing tech work in millimeters and now
  we go backwards. Damn. But if that's what the manufacturers use,
  well I guess I'm stuck with it. It still begs the question: Should I
  have also started out by specifying a Page Layout using US Letter
  8.5" x 11" rather than the A4 paper size in mm?

* **Annotate** numbers all the parts at once. Very quick.

* We associated the cached symbols as a project-specific library in
  the preferences.

* The symbol icon on the *right* toolbar is for *placing symbols* The
  symbol icon on the *top* toolbar is for *editing symbols*.

* When all is said and done from Lesson 1, we have:

        -rw-rw-r-- 1  2932943 Jul  6 19:58 fp-info-cache
        -rw-rw-r-- 1     3187 Jul  6 23:34 power_supply.sch
        -rw-rw-r-- 1     2683 Jul  6 23:26 power_supply.sch-bak
        -rw-rw-r-- 1      219 Jul  7 07:37 sym-lib-table
        -rw-rw-r-- 1    54993 Jul  6 23:49 tiny_blinky.pdf
        -rw-rw-r-- 1       48 Jul  7 07:45 tiny_blinky.bck
        -rw-rw-r-- 1     6686 Jul  7 07:45 tiny_blinky-cache.bck
        -rw-rw-r-- 1       48 Jul  7 07:45 tiny_blinky-cache.dcm
        -rw-rw-r-- 1     6596 Jul  7 07:45 tiny_blinky-cache.lib
        -rw-rw-r-- 1       48 Jul  7 07:45 tiny_blinky.dcm
        -rw-rw-r-- 1       51 Jul  6 19:38 tiny_blinky.kicad_pcb
        -rw-rw-r-- 1     2424 Jul  7 07:45 tiny_blinky.lib
        -rw-r--r-- 1      881 Jul  6 23:50 tiny_blinky.pro
        -rw-rw-r-- 1     7457 Jul  6 23:34 tiny_blinky.sch
        -rw-rw-r-- 1     7457 Jul  6 23:26 tiny_blinky.sch-bak

* [Notes: KiCad + FreeCAD Class
  1](https://hackaday.io/project/172360-introduction-to-kicad-and-freecad/log/180356-kicad-freecad-class-1-notes)

----

## 2020.07.14

* I don't know what he's talking about regarding the improved **bus**
  stuff he refers to as being in the notes from class 1 above.

* `logo_hacdc.lib` and `logo_hacdc.kicad_mod` are located in 
  `~/Learn/Electronics/KiCad/` and generated from an unscaled
  `logo_168.png`.

----

## 2020.08.08 KJC

### Importing to Blender!

Anool [tweeted](https://twitter.com/anool/status/1292100298589126662):

> I just started learning Blender this week after putting it off for
> over 5 years. TL;DR

* Export WRL from KiCad
* Use http://cadexchanger.com to convert WRL > DAE (collada)
* Import DAE in Blender and setup lighting, layer colors etc.

----

