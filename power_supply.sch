EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L tiny_blinky:Battery BT1
U 1 1 5F0CE184
P 2800 3400
F 0 "BT1" H 2908 3446 50  0000 L CNN
F 1 "CR2032x2" H 2908 3355 50  0000 L CNN
F 2 "tiny_blinky:BatteryHolder_Keystone_3024-2_1x2032" V 2800 3460 50  0001 C CNN
F 3 "~" V 2800 3460 50  0001 C CNN
	1    2800 3400
	1    0    0    -1  
$EndComp
$Comp
L tiny_blinky:CP C1
U 1 1 5F0CE659
P 3400 3400
F 0 "C1" H 3518 3446 50  0000 L CNN
F 1 "470uF/25V" H 3518 3355 50  0000 L CNN
F 2 "tiny_blinky:CP_Radial_D8.0mm_P3.50mm" H 3438 3250 50  0001 C CNN
F 3 "~" H 3400 3400 50  0001 C CNN
	1    3400 3400
	1    0    0    -1  
$EndComp
$Comp
L tiny_blinky:C C2
U 1 1 5F0CE777
P 4800 3400
F 0 "C2" H 4915 3446 50  0000 L CNN
F 1 "100nF" H 4915 3355 50  0000 L CNN
F 2 "tiny_blinky:C_Disc_D5.0mm_W2.5mm_P2.50mm" H 4838 3250 50  0001 C CNN
F 3 "~" H 4800 3400 50  0001 C CNN
	1    4800 3400
	1    0    0    -1  
$EndComp
$Comp
L tiny_blinky:AP1117-50 U2
U 1 1 5F0D5B7C
P 4200 2950
F 0 "U2" H 4200 3192 50  0000 C CNN
F 1 "AP1117-50" H 4200 3101 50  0000 C CNN
F 2 "tiny_blinky:SOT-223-3_TabPin2" H 4200 3150 50  0001 C CNN
F 3 "http://www.diodes.com/datasheets/AP1117.pdf" H 4300 2700 50  0001 C CNN
	1    4200 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	2300 3350 2500 3350
Wire Wire Line
	2500 3350 2500 2950
Wire Wire Line
	2500 2950 2800 2950
Wire Wire Line
	4800 3550 4800 3800
Wire Wire Line
	4800 3800 4200 3800
Wire Wire Line
	2500 3800 2500 3450
Wire Wire Line
	2500 3450 2300 3450
Wire Wire Line
	2800 3200 2800 2950
Connection ~ 2800 2950
Wire Wire Line
	2800 2950 3400 2950
Wire Wire Line
	3400 3250 3400 2950
Connection ~ 3400 2950
Wire Wire Line
	3400 2950 3900 2950
Wire Wire Line
	2800 3600 2800 3800
Connection ~ 2800 3800
Wire Wire Line
	2800 3800 2500 3800
Wire Wire Line
	3400 3550 3400 3800
Connection ~ 3400 3800
Wire Wire Line
	3400 3800 2800 3800
Wire Wire Line
	4200 3250 4200 3800
Connection ~ 4200 3800
Wire Wire Line
	4200 3800 3400 3800
Text HLabel 1650 3350 0    50   Output ~ 0
5V
Text HLabel 1650 3450 0    50   Output ~ 0
GND
Wire Wire Line
	1650 3350 1850 3350
Wire Wire Line
	1650 3450 1850 3450
Text Label 1850 3350 2    50   ~ 0
5V
Text Label 1850 3450 2    50   ~ 0
GND
Text Label 4800 3800 2    50   ~ 0
GND
Text Label 2500 2950 0    50   ~ 0
Vin
$Comp
L tiny_blinky:PWR_FLAG #FLG01
U 1 1 5F0ED8ED
P 2800 2950
F 0 "#FLG01" H 2800 3025 50  0001 C CNN
F 1 "PWR_FLAG" H 2800 3123 50  0000 C CNN
F 2 "" H 2800 2950 50  0001 C CNN
F 3 "~" H 2800 2950 50  0001 C CNN
	1    2800 2950
	1    0    0    -1  
$EndComp
$Comp
L tiny_blinky:PWR_FLAG #FLG02
U 1 1 5F0EDF9C
P 2800 3800
F 0 "#FLG02" H 2800 3875 50  0001 C CNN
F 1 "PWR_FLAG" H 2800 3973 50  0000 C CNN
F 2 "" H 2800 3800 50  0001 C CNN
F 3 "~" H 2800 3800 50  0001 C CNN
	1    2800 3800
	-1   0    0    1   
$EndComp
$Comp
L tiny_blinky:Conn_01x02_Male J3
U 1 1 5F1268FF
P 2100 3350
F 0 "J3" H 2208 3531 50  0000 C CNN
F 1 "Vin" H 2208 3440 50  0000 C CNN
F 2 "tiny_blinky:PinSocket_1x02_P2.54mm_Vertical" H 2100 3350 50  0001 C CNN
F 3 "~" H 2100 3350 50  0001 C CNN
	1    2100 3350
	1    0    0    -1  
$EndComp
$Comp
L tiny_blinky:Conn_01x02_Female J4
U 1 1 5F12A25A
P 5600 3350
F 0 "J4" H 5628 3326 50  0000 L CNN
F 1 "5V" H 5628 3235 50  0000 L CNN
F 2 "tiny_blinky:PinSocket_1x02_P2.54mm_Vertical" H 5600 3350 50  0001 C CNN
F 3 "~" H 5600 3350 50  0001 C CNN
	1    5600 3350
	1    0    0    -1  
$EndComp
Text Label 4800 2950 2    50   ~ 0
5V
Wire Wire Line
	4800 2950 4800 3250
Wire Wire Line
	4500 2950 4800 2950
Wire Wire Line
	5400 3350 5400 2950
Wire Wire Line
	5400 2950 4800 2950
Connection ~ 4800 2950
Wire Wire Line
	5400 3450 5400 3800
Wire Wire Line
	5400 3800 4800 3800
Connection ~ 4800 3800
$EndSCHEMATC
